﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Networking_Core
{
    public static class Networking
    {
        public static void SendPacketTCP(NetworkStream stream, DataPacket packetToSend)
        {
            try
            {
                MemoryStream memStream = new MemoryStream();

                new BinaryFormatter().Serialize(memStream, packetToSend);
                memStream.WriteTo(stream);
                memStream.SetLength(0);
            }
            catch (Exception e) { }
        }

        public static DataPacket ReceivePacketTCP(NetworkStream stream)
        {
            DataPacket packetReceived = new DataPacket();
            try
            {
                return new BinaryFormatter().Deserialize(stream) as DataPacket;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static void SendPacketUDP(DataPacket data, Socket sending_socket, ref IPEndPoint endPoint)
        {
            try
            {
                MemoryStream memStream = new MemoryStream();
                new BinaryFormatter().Serialize(memStream, data);
                sending_socket.SendTo(memStream.ToArray(), endPoint);
            }
            catch (Exception e) { }
        }

        public static DataPacket ReceivePacketUDP(UdpClient server, ref IPEndPoint endPoint)
        {
            try
            {
                byte[] receiveBytes = new byte[1024];
                receiveBytes = server.Receive(ref endPoint);

                MemoryStream memStream = new MemoryStream(receiveBytes);
                memStream.Seek(0, SeekOrigin.Begin);
                return new BinaryFormatter().Deserialize(memStream) as DataPacket;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
