﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Networking_Core
{
    [Serializable]
    public class ChatMessagePacket : DataPacket
    {
        public string name;
        public string time;
        public string message;

        public ChatMessagePacket() { }
        public ChatMessagePacket(string name, string time, string message)
        { this.name = name; this.time = time; this.message = message; }
    }
}
