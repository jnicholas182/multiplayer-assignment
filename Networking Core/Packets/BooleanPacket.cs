﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Networking_Core
{
    [Serializable]
    public class BooleanPacket : DataPacket
    {
        public bool boolean;
        public int id;
        public BooleanPacket() { }
        public BooleanPacket(bool boolean)
        { this.boolean = boolean; }
        public BooleanPacket(bool boolean, int id)
        { this.boolean = boolean; this.id = id; }
    }
}
