﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Networking_Core
{
    [Serializable]
    public class WindowMessagePacket : DataPacket
    {
        public string title;
        public string message;

        public WindowMessagePacket() { }
        public WindowMessagePacket(string title, string message)
        { this.title = title; this.message = message; }
    }
}
