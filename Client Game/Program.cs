using System;
using System.Collections.Generic;
using System.Linq;

namespace Client_Game
{
    public static class Program
    {
        [STAThread]
        static void Main(string []args)
        {
            using (var game = new ClientGame(args[0], Convert.ToInt32(args[1]), Convert.ToInt32(args[2]), Convert.ToInt32(args[3])))
                game.Start();
        }
    }
}
