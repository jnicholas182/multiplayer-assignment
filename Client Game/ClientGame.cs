using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Networking_Core;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

namespace Client_Game
{
    public class ClientGame : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D playerCircle, opponentCircle, EscMessage;
        int playerX, playerY, opponentX, opponentY;

        int ID;
        string hostname;
        int sendPort, receivePort;
        UdpClient listener;
        Socket sendingSocket;
        IPEndPoint sendingEP, receivingEP;

        MouseState prevMouseState;
        bool gameOver;

        public ClientGame(string hostname, int ID, int sendPort, int receivePort)
        {
            this.hostname = hostname;
            this.ID = ID;
            this.sendPort = sendPort;
            this.receivePort = receivePort;
            graphics = new GraphicsDeviceManager(this);
            //Window.IsBorderless = true;
            Content.RootDirectory = "Content";
            gameOver = false;
        }

        public void Start()
        {
            sendingSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            sendingEP = new IPEndPoint(IPAddress.Parse(hostname), sendPort);

            listener = new UdpClient(receivePort);
            receivingEP = new IPEndPoint(IPAddress.Any, receivePort);

            Thread gameTraffic = new Thread(new ThreadStart(HandleGameData));
            gameTraffic.IsBackground = true;
            gameTraffic.Start();            
            Run();
        }

        void HandleGameData()
        {
            while (true)
            {
                try
                {
                    DataPacket data = Networking.ReceivePacketUDP(listener, ref receivingEP);
                    if (data.GetType() == typeof(PlayerPositionPacket))
                    {
                        setOpponent((data as PlayerPositionPacket).position);
                    }
                    else if (data.GetType() == typeof(BooleanPacket))
                    {
                        gameOver = true;
                        base.Exit();
                        break;
                    }
                }
                catch(Exception e)
                {
                    MessageBox.Show(e.Message, "Receiving Data");
                    base.Exit();
                }
            }
            base.Exit();
        }

        public void setOpponent(Client_Game.Vector2 opponentPos)
        {
            opponentX = (int)opponentPos.x;
            opponentY = (int)opponentPos.y;
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            playerCircle = CreateCircle(10);
            opponentCircle = CreateCircle(10);
            EscMessage = Content.Load<Texture2D>(@"EscMessage.png");
        }

        protected override void UnloadContent() {}

        protected override void Update(GameTime gameTime)
        {
            if (!gameOver)
            {
                if (IsActive)
                {
                    MouseState mouseState = Mouse.GetState();
                    if (mouseState != prevMouseState)
                    {
                        prevMouseState = mouseState;
                        playerX = mouseState.X;
                        playerY = mouseState.Y;
                        BoundPlayersToWindow();

                        try
                        {
                            Networking.SendPacketUDP(new PlayerPositionPacket(ID, new Client_Game.Vector2((float)playerX, (float)playerY)), sendingSocket, ref sendingEP);
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(e.Message, "Sending Data");
                            base.Exit();
                        }
                    }
                    KeyboardState keyboardState = Keyboard.GetState();
                    if (keyboardState.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Escape))
                    {
                        base.Exit();
                    }
                    base.Update(gameTime);
                }
            }
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            if(!gameOver) 
                Networking.SendPacketUDP(new BooleanPacket(false, ID), sendingSocket, ref sendingEP);
            gameOver = true;
            base.OnExiting(sender, args);
        }

        private void BoundPlayersToWindow()
        {
            if (playerX <= Window.ClientBounds.X)
                playerX = Window.ClientBounds.X;
            if (playerX >= Window.ClientBounds.Width - playerCircle.Width)
                playerX = Window.ClientBounds.Width - playerCircle.Width;
            if (playerY <= Window.ClientBounds.Y)
                playerY = Window.ClientBounds.Y;
            if (playerY >= Window.ClientBounds.Height - playerCircle.Height)
                playerY = Window.ClientBounds.Height - playerCircle.Height;
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();

            spriteBatch.Draw(EscMessage, new Microsoft.Xna.Framework.Rectangle(0, 0, 150, 22), Color.White); ;
            spriteBatch.Draw(playerCircle, new Microsoft.Xna.Framework.Vector2(playerX, playerY), Color.Cyan);
            spriteBatch.Draw(opponentCircle, new Microsoft.Xna.Framework.Vector2(opponentX, opponentY), Color.Magenta);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        public Texture2D CreateCircle(int radius)
        {
            int outerRadius = radius * 2 + 2;
            Texture2D texture = new Texture2D(GraphicsDevice, outerRadius, outerRadius);

            Color[] data = new Color[outerRadius * outerRadius];

            for (int i = 0; i < data.Length; i++)
                data[i] = Color.Transparent;

            double angleStep = 1.0f / radius;

            for (double angle = 0; angle < Math.PI * 2; angle += angleStep)
            {
                int x = (int)Math.Round(radius + radius * Math.Cos(angle));
                int y = (int)Math.Round(radius + radius * Math.Sin(angle));

                data[y*outerRadius + x + 1] = Color.White;
            }

            texture.SetData(data);
            return texture;
        }
    }
}
