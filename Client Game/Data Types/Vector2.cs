﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Networking_Core;

namespace Client_Game
{
    [Serializable]
    public class Vector2
    {
        public float x;
        public float y;
        public Vector2() { }
        public Vector2(float x, float y)
        { this.x = x; this.y = y; }
    };
}
