﻿using Networking_Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client_Game
{
    [Serializable]
    public class PlayerPositionPacket : DataPacket
    {
        public int ID;
        public Vector2 position;

        public PlayerPositionPacket() { }

        public PlayerPositionPacket(Vector2 position)
        { this.position = position; }

        public PlayerPositionPacket(int ID, Vector2 position)
        { this.ID = ID; ; this.position = position; }
    }
}
