﻿namespace GUI_Client
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.playerList = new System.Windows.Forms.DataGridView();
            this.Column_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_State = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button_invite = new System.Windows.Forms.Button();
            this.button_logout = new System.Windows.Forms.Button();
            this.messageBox = new System.Windows.Forms.TextBox();
            this.button_sendMessage = new System.Windows.Forms.Button();
            this.button_help = new System.Windows.Forms.Button();
            this.chatWindow = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.playerList)).BeginInit();
            this.SuspendLayout();
            // 
            // playerList
            // 
            this.playerList.AllowUserToAddRows = false;
            this.playerList.AllowUserToDeleteRows = false;
            this.playerList.AllowUserToResizeColumns = false;
            this.playerList.AllowUserToResizeRows = false;
            this.playerList.BackgroundColor = System.Drawing.Color.Black;
            this.playerList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.playerList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_Name,
            this.Column_State});
            this.playerList.GridColor = System.Drawing.Color.Black;
            this.playerList.Location = new System.Drawing.Point(489, 12);
            this.playerList.Name = "playerList";
            this.playerList.ReadOnly = true;
            this.playerList.RowHeadersVisible = false;
            this.playerList.ShowEditingIcon = false;
            this.playerList.Size = new System.Drawing.Size(219, 209);
            this.playerList.TabIndex = 3;
            this.playerList.SelectionChanged += new System.EventHandler(this.playerList_SelectionChanged);
            // 
            // Column_Name
            // 
            this.Column_Name.HeaderText = "Name";
            this.Column_Name.Name = "Column_Name";
            this.Column_Name.ReadOnly = true;
            this.Column_Name.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column_Name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column_Name.Width = 155;
            // 
            // Column_State
            // 
            this.Column_State.HeaderText = "";
            this.Column_State.Name = "Column_State";
            this.Column_State.ReadOnly = true;
            this.Column_State.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column_State.Width = 60;
            // 
            // button_invite
            // 
            this.button_invite.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_invite.Location = new System.Drawing.Point(498, 227);
            this.button_invite.Name = "button_invite";
            this.button_invite.Size = new System.Drawing.Size(86, 42);
            this.button_invite.TabIndex = 4;
            this.button_invite.Text = "Invite";
            this.button_invite.UseVisualStyleBackColor = true;
            this.button_invite.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_startGame_MouseUp);
            // 
            // button_logout
            // 
            this.button_logout.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_logout.Location = new System.Drawing.Point(613, 227);
            this.button_logout.Name = "button_logout";
            this.button_logout.Size = new System.Drawing.Size(86, 42);
            this.button_logout.TabIndex = 5;
            this.button_logout.Text = "Logout";
            this.button_logout.UseVisualStyleBackColor = true;
            this.button_logout.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_quit_MouseUp);
            // 
            // messageBox
            // 
            this.messageBox.BackColor = System.Drawing.Color.Black;
            this.messageBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.messageBox.ForeColor = System.Drawing.Color.White;
            this.messageBox.Location = new System.Drawing.Point(119, 268);
            this.messageBox.Name = "messageBox";
            this.messageBox.Size = new System.Drawing.Size(351, 21);
            this.messageBox.TabIndex = 7;
            // 
            // button_sendMessage
            // 
            this.button_sendMessage.Location = new System.Drawing.Point(29, 265);
            this.button_sendMessage.Name = "button_sendMessage";
            this.button_sendMessage.Size = new System.Drawing.Size(75, 23);
            this.button_sendMessage.TabIndex = 8;
            this.button_sendMessage.Text = "Send";
            this.button_sendMessage.UseVisualStyleBackColor = true;
            this.button_sendMessage.Click += new System.EventHandler(this.button_sendMessage_Click);
            // 
            // button_help
            // 
            this.button_help.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.button_help.Location = new System.Drawing.Point(560, 275);
            this.button_help.Name = "button_help";
            this.button_help.Size = new System.Drawing.Size(75, 35);
            this.button_help.TabIndex = 9;
            this.button_help.Text = "Help";
            this.button_help.UseVisualStyleBackColor = true;
            this.button_help.Click += new System.EventHandler(this.button_help_Click);
            // 
            // chatWindow
            // 
            this.chatWindow.BackColor = System.Drawing.Color.Black;
            this.chatWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chatWindow.Location = new System.Drawing.Point(29, 12);
            this.chatWindow.Name = "chatWindow";
            this.chatWindow.ReadOnly = true;
            this.chatWindow.Size = new System.Drawing.Size(441, 235);
            this.chatWindow.TabIndex = 10;
            this.chatWindow.Text = "";
            // 
            // MainForm
            // 
            this.AcceptButton = this.button_sendMessage;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(765, 322);
            this.Controls.Add(this.chatWindow);
            this.Controls.Add(this.button_help);
            this.Controls.Add(this.button_sendMessage);
            this.Controls.Add(this.messageBox);
            this.Controls.Add(this.button_logout);
            this.Controls.Add(this.button_invite);
            this.Controls.Add(this.playerList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Game Lobby";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.playerList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView playerList;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_State;
        private System.Windows.Forms.Button button_invite;
        private System.Windows.Forms.Button button_logout;
        private System.Windows.Forms.TextBox messageBox;
        private System.Windows.Forms.Button button_sendMessage;
        private System.Windows.Forms.Button button_help;
        private System.Windows.Forms.RichTextBox chatWindow;


    }
}

