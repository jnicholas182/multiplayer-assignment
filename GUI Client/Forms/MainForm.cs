﻿using GUI_Client.Forms;
using Networking_Core;
using Server_Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI_Client
{
    public partial class MainForm : Form
    {
        private TcpClient _tcpClient;
        private NetworkStream netStream;
        private string name;
        private gameState state;
        private string hostname;
        private Process game;
        public MainForm()
        {
            Application.ApplicationExit += new EventHandler(this.OnApplicationExit);
            InitializeComponent();
            state = gameState.Lobby;
        }

        public bool Connect()
        {
            ConnectForm connectForm = new ConnectForm();
            tryconnect:
            if (connectForm.ShowDialog() == DialogResult.OK)
            {
                _tcpClient = new TcpClient();

                try
                {
                    _tcpClient.Connect(connectForm.Hostname, Convert.ToInt32(connectForm.Port));
                    netStream = _tcpClient.GetStream();

                    if ((Networking.ReceivePacketTCP(netStream) as BooleanPacket).boolean)
                    {
                        UsernameForm usernameForm = new UsernameForm();
                        if (usernameForm.ShowDialog() == DialogResult.OK)
                        {
                            Networking.SendPacketTCP(netStream, new UserJoinPacket(usernameForm.Username));

                            while (!(Networking.ReceivePacketTCP(netStream) as BooleanPacket).boolean)
                            {
                                MessageBox.Show("Username is already taken! Enter a different username.");
                                if (usernameForm.ShowDialog() == DialogResult.OK)
                                {
                                    Networking.SendPacketTCP(netStream, new UserJoinPacket(usernameForm.Username));
                                }
                            }
                            this.name = usernameForm.Username;
                            this.hostname = connectForm.Hostname;
                        }
                        return true;
                    }
                    else
                    {
                        this.Hide();
                        DialogResult result = MessageBox.Show("Sorry, the Lobby is full! Please try again later", "Lobby Is Full!", MessageBoxButtons.OK);
                        if (result == DialogResult.OK)
                        {
                            this.Close();
                        }
                        return false;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to connect to: " + connectForm.Hostname + ":" + connectForm.Port);
                    goto tryconnect;
                }
            }
            else
            {
                return false;
            }
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            new Thread(new ThreadStart(ReceiveMessage)).Start(); 
        }

        private void ReceiveMessage()
        {
            try
            {
                while (this.Created)
                {
                    DataPacket data = Networking.ReceivePacketTCP(netStream);
                    if (data == null)
                        Invoke(new ProcessServerLostDelegate(processServerLost));
                    else if (data.GetType() == typeof(InvitePacket))
                        Invoke(new ProcessInvitePacketDelegate(ProcessInvitePacket), (data as InvitePacket).opponent);
                    else if (data.GetType() == typeof(ChatMessagePacket))
                        Invoke(new ProcessChatMessageDelegate(ProcessChatMessage), (data as ChatMessagePacket));
                    else if (data.GetType() == typeof(WindowMessagePacket))
                        Invoke(new ProcessWindowMessageDelegate(ProcessWindowMessage), (data as WindowMessagePacket));
                    else if (data.GetType() == typeof(PlayerListPacket))
                        Invoke(new updateItemsDelegate(updateList), (data as PlayerListPacket).playerList);
                    else if (data.GetType() == typeof(GameInstanceDataPacket))
                        Invoke(new ProcessGameInstanceStartDelegate(ProcessGameInstanceStart), (data as GameInstanceDataPacket));
                    else if (data.GetType() == typeof(ChangeStatePacket))
                        Invoke(new UpdateStateDelegate(UpdateState), (data as ChangeStatePacket).state);
                    Application.DoEvents();
                }
            }
            catch (Exception e) { string message = e.Message; }
        }

        delegate void ProcessServerLostDelegate();
        void processServerLost()
        {
            this.Hide();
            DialogResult result = MessageBox.Show("Connection to server was lost! Please try reconnecting", "Connection Lost!", MessageBoxButtons.OK);
            if (result == DialogResult.OK)
                this.Close();
        }

        delegate void ProcessGameInstanceStartDelegate(GameInstanceDataPacket gameInstanceDataPacket);
        void ProcessGameInstanceStart(GameInstanceDataPacket gameInstanceDataPacket)
        {
            this.state = gameState.Game;
            Process game = new Process();   
            game.StartInfo.FileName = "Client Game.exe";    
            game.StartInfo.Arguments = hostname + " " + gameInstanceDataPacket.ID.ToString() + " " + gameInstanceDataPacket.sendPort.ToString() + " " + gameInstanceDataPacket.receivePort.ToString();
            game.EnableRaisingEvents = true;

            game.Exited += (sender, e) =>
            {
                Networking.SendPacketTCP(netStream, new ChangeStatePacket(gameState.Lobby));
            };

            game.Start();
        }

        delegate void ProcessInvitePacketDelegate(string opponent);
        void ProcessInvitePacket(string opponent)
        {
            DialogResult result = MessageBox.Show(opponent + " has invited you to a game! Do you accept?", "Invite!", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
                Networking.SendPacketTCP(netStream, new BooleanPacket(true));
            else if (result == DialogResult.No)
                Networking.SendPacketTCP(netStream, new BooleanPacket(false));
        }

        delegate void ProcessWindowMessageDelegate(WindowMessagePacket windowMessagePacket);
        void ProcessWindowMessage(WindowMessagePacket windowMessagePacket)
        {
            MessageBox.Show(windowMessagePacket.message, windowMessagePacket.title, MessageBoxButtons.OK);
        }

        delegate void ProcessChatMessageDelegate(ChatMessagePacket chatMessagePacket);
        void ProcessChatMessage(ChatMessagePacket chatMessagePacket)
        {
            string message = "[" + chatMessagePacket.time + "] " + chatMessagePacket.name + ": " + chatMessagePacket.message + "\n";
            int length = chatWindow.TextLength;
            chatWindow.AppendText(message);
            chatWindow.SelectionStart = length;
            chatWindow.SelectionLength = message.Length;

            if (chatMessagePacket.name == null)
                chatWindow.SelectionColor = Color.LimeGreen;
            else if (chatMessagePacket.name == name)
                chatWindow.SelectionColor = Color.Cyan;
            else
                chatWindow.SelectionColor = Color.Magenta;

            chatWindow.SelectionStart = chatWindow.Text.Length;
            chatWindow.ScrollToCaret();
        }

        delegate void UpdateStateDelegate(gameState state);
        void UpdateState(gameState state)
        {
            switch (state)
            {
                case gameState.Lobby:
                    if (this.state == gameState.Game)
                    {
                        MessageBox.Show("Game was closed by opponent!\nReturning to the Lobby", "Game Closed!", MessageBoxButtons.OK);
                    }
                    this.state = gameState.Lobby;
                    break;

                case gameState.Game:
                    this.state = gameState.Game;
                    break;

                case gameState.Quit:
                    this.Close();
                    break;
            }
        }

        delegate void updateItemsDelegate(List<Player> players);
        void updateList(List<Player> players)
        {
            playerList.Rows.Clear();
            foreach(Player player in players)
            {
                playerList.Rows.Add(new string[] { player.name, "" });
                playerList.Rows[playerList.Rows.Count - 1].Cells[0].Style.BackColor = Color.Black;
                playerList.Rows[playerList.Rows.Count - 1].Cells[0].Style.ForeColor = Color.White;
                playerList.Rows[playerList.Rows.Count - 1].Cells[0].Style.Font = new Font("Arial", 10, FontStyle.Bold);
                if (player.name == name)
                {
                    state = player.state;
                    playerList.Rows[playerList.Rows.Count - 1].Cells[0].Style.ForeColor = Color.Cyan;
                }
                if(player.state == gameState.Lobby)
                    playerList.Rows[playerList.Rows.Count - 1].Cells[1].Style.BackColor = Color.LimeGreen;
                else if(player.state == gameState.Pending)
                    playerList.Rows[playerList.Rows.Count - 1].Cells[1].Style.BackColor = Color.Yellow;
                else if (player.state == gameState.Game)
                    playerList.Rows[playerList.Rows.Count - 1].Cells[1].Style.BackColor = Color.Magenta;
            }
            playerList.ClearSelection();
        }

        private void playerList_SelectionChanged(object sender, EventArgs e)
        {
            if (playerList.CurrentCell.ColumnIndex == 1 || playerList.CurrentCell.Value.ToString() == name
                || playerList.CurrentRow.Cells[1].Style.BackColor == Color.Magenta || playerList.CurrentRow.Cells[1].Style.BackColor == Color.Yellow)
                playerList.CurrentCell.Selected = false;
        }

        private void button_startGame_MouseUp(object sender, MouseEventArgs e)
        {
            if (!playerList.CurrentCell.Selected)
                MessageBox.Show("No Opponent Selected!");
            else if (state == gameState.Lobby)
            {
                Networking.SendPacketTCP(netStream, new InvitePacket(playerList.CurrentCell.Value.ToString()));
                MessageBox.Show("Invite Sent!");
            }
            else if(state == gameState.Pending)
                MessageBox.Show("You cannot send multiple invites!");
            else if(state == gameState.Game)
                MessageBox.Show("One game at a time!");
        }

        private void button_quit_MouseUp(object sender, MouseEventArgs e)
        {
            this.Close();
        }

        private void OnApplicationExit(object sender, EventArgs e)
        {
            try { game.Kill(); }
            catch (Exception) { }
            
            try
            {
                Networking.SendPacketTCP(netStream, new ChangeStatePacket(gameState.Quit));
                _tcpClient.Close();
            }
            catch (Exception) { }
        }

        private void button_sendMessage_Click(object sender, EventArgs e)
        {
            if (messageBox.Text.Trim().Length > 0)
            {
                Networking.SendPacketTCP(netStream, new ChatMessagePacket(this.name, DateTime.Now.ToString("h:mm:ss"), messageBox.Text));
                messageBox.Clear();
            }
        }

        private void button_help_Click(object sender, EventArgs e)
        {
            HelpForm helpForm = new HelpForm();
            helpForm.ShowDialog();
        }
    }
}
