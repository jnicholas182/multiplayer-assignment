﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rings_Server
{
    class Program
    {
        static void Main(string[] args)
        {
            RingsServer server = new RingsServer("ServerConfig.xml");
            server.Start();
            server.Stop();
        }
    }
}
