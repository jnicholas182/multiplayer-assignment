﻿using Server_Core;
using Networking_Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Rings_Server
{
    public class RingsServer : BaseServer<RingsGame>
    {
        public RingsServer(string filename) : base(filename)
        {
            lobby = new RingsLobby(this);
        }

        protected override void ReceiveMessage(object obj)
        {
            Player player = obj as Player;
            try
            {
                while (true)
                {
                    DataPacket data = Networking.ReceivePacketTCP(new NetworkStream(player.socket, true));
                    lobby.HandleLobbyPacket(data, player);
                }
            }
            catch (Exception e)
            {

            }
        }
    }
}
