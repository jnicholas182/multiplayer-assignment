﻿using Server_Core;
using Networking_Core;
using Client_Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Rings_Server
{
    public class RingsGame : BaseGame
    {
        private RingsLobby lobby;
        public RingsGame(Player instigator, Player opponent) : base(instigator, opponent) { }

        public void Start(int receivePort, RingsLobby lobby)
        {
            this.lobby = lobby;
            base.Start(receivePort);
        }

        protected override void ReceiveMessage()
        {          
            while (!gameOver)
            {
                try
                {
                    DataPacket data = Networking.ReceivePacketUDP(listener, ref receiveEP);
                    if (data.GetType() == typeof(PlayerPositionPacket))
                        UpdatePlayerPosition(data as PlayerPositionPacket);
                    else if (data.GetType() == typeof(BooleanPacket))
                        ShutdownGame((data as BooleanPacket).id);
                }
                catch(Exception e)
                {
                    break;
                }
            }
        }

        private void UpdatePlayerPosition(PlayerPositionPacket playerGameData)
        {
            if (playerGameData.ID == 1)
            {
                Networking.SendPacketUDP(new PlayerPositionPacket(playerGameData.position), sendingSocket, ref sendPlayer2EP);
            }
            else if(playerGameData.ID == 2)
            {
                Networking.SendPacketUDP(new PlayerPositionPacket(playerGameData.position), sendingSocket, ref sendPlayer1EP);
            }
        }

        public void ShutdownGame(int id)
        {
            gameOver = true;

            if (id == 1)
            {
                Networking.SendPacketUDP(new BooleanPacket(false), sendingSocket, ref sendPlayer2EP);
                Networking.SendPacketTCP(new NetworkStream(opponent.socket, true), new ChangeStatePacket(gameState.Lobby));
            }
            else
            {
                Networking.SendPacketUDP(new BooleanPacket(false), sendingSocket, ref sendPlayer1EP);
                Networking.SendPacketTCP(new NetworkStream(instigator.socket, true), new ChangeStatePacket(gameState.Lobby));
            }
            listener.Close();

            instigator.state = gameState.Lobby;
            opponent.state = gameState.Lobby;
            lobby.RefreshPlayerList();
        }
    }
}
