﻿using Server_Core;
using Networking_Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Rings_Server
{
    public class RingsLobby : BaseLobby<RingsGame>
    {
        public RingsLobby(RingsServer server) : base(server) { }

        public override void HandleLobbyPacket(DataPacket data, Player player)
        {
            if (data.GetType() == typeof(InvitePacket))
                StartGame(player, data as InvitePacket);
            else if (data.GetType() == typeof(ChangeStatePacket))
                HandlePlayerChangeState(data as ChangeStatePacket, player);
            else if (data.GetType() == typeof(BooleanPacket))
                HandleBooleanPacket((data as BooleanPacket), player);
            else if (data.GetType() == typeof(ChatMessagePacket))
                DistributeMessage(data as ChatMessagePacket);
        }

        protected override void HandleBooleanPacket(BooleanPacket booleanPacket, Player player)
        {
            if (booleanPacket.boolean)
            {
                if (player.state == gameState.Pending)
                    player.state = gameState.Game;
            }
            else
            {
                if (player.state == gameState.Pending)
                    player.state = gameState.Lobby;
            }
        }

        protected override void HandlePlayerChangeState(ChangeStatePacket changeStatePacket, Player player)
        {
            if(changeStatePacket.state == gameState.Lobby)
            {
                for (int i = 0; i < gameList.Count; i++)
                {
                    if (player == gameList[i].instigator || player == gameList[i].opponent)
                    {
                        DistributeMessage(new ChatMessagePacket(null, DateTime.Now.ToString("h:mm:ss"), gameList[i].instigator.name + " and " + gameList[i].opponent.name + " have returned to the lobby!"));
                        try
                        {
                            gameList.RemoveAt(i);
                        }
                        catch (Exception) { }
                        break;
                    }
                }
            }
            if(changeStatePacket.state == gameState.Quit && player.state == gameState.Game)
            {
                PlayerLostInGame(player);
            }

            else if(changeStatePacket.state == gameState.Quit && player.state == gameState.Lobby)
            {
                player.state = gameState.Quit;
                DistributeMessage(new ChatMessagePacket(null, DateTime.Now.ToString("h:mm:ss"), player.name + " left the lobby!"));
                playerList.Remove(player);

                RefreshPlayerList();
            }
        }

        private void PlayerLostInGame(Player player)
        {
            for (int i = 0; i < gameList.Count; i++)
            {
                if (player == gameList[i].instigator)
                {
                    gameList[i].ShutdownGame(1);
                    DistributeMessage(new ChatMessagePacket(null, DateTime.Now.ToString("h:mm:ss"), player.name + " left the lobby!"));
                    DistributeMessage(new ChatMessagePacket(null, DateTime.Now.ToString("h:mm:ss"), gameList[i].opponent.name + " returned to the lobby!"));
                    try
                    {
                        gameList.RemoveAt(i);
                    }
                    catch (Exception) { }
                    playerList.Remove(player);
                    RefreshPlayerList();
                    break;
                }
                else if (player == gameList[i].opponent)
                {
                    gameList[i].ShutdownGame(2);
                    DistributeMessage(new ChatMessagePacket(null, DateTime.Now.ToString("h:mm:ss"), player.name + " left the lobby!"));
                    DistributeMessage(new ChatMessagePacket(null, DateTime.Now.ToString("h:mm:ss"), gameList[i].instigator.name + " returned to the lobby!"));
                    try
                    {
                        gameList.RemoveAt(i);
                    }
                    catch (Exception) { }
                    playerList.Remove(player);
                    RefreshPlayerList();
                    break;
                }
            }
        }

        protected override void StartGame(Player player, InvitePacket requestGameStartPacket)
        {
            foreach (Player opponent in playerList)
            {
                if (opponent.name == requestGameStartPacket.opponent)
                {
                    player.state = gameState.Pending;
                    opponent.state = gameState.Pending;
                    RefreshPlayerList();
                    Networking.SendPacketTCP(new NetworkStream(opponent.socket, true), new InvitePacket(player.name));
                    while (opponent.state == gameState.Pending) { }

                    //Check if either player dropped connection during invite
                    if (opponent.state == gameState.Quit || player.state == gameState.Quit)
                    {
                        if (opponent.state == gameState.Quit)
                        {
                            player.state = gameState.Lobby;
                            RefreshPlayerList();
                            Networking.SendPacketTCP(new NetworkStream(player.socket, true), new WindowMessagePacket("Connection Error", "Opponent's connection was lost during invitation process\nReturning to lobby"));
                        }
                        if (player.state == gameState.Quit)
                        {
                            opponent.state = gameState.Lobby;
                            RefreshPlayerList();
                            Networking.SendPacketTCP(new NetworkStream(opponent.socket, true), new WindowMessagePacket("Connection Error", "Opponent's connection was lost during invitation process\nReturning to lobby"));
                        }
                    }
                    else
                    {
                        //Start a game
                        if (opponent.state == gameState.Game)
                        {
                            player.state = gameState.Game;
                            RefreshPlayerList();
                            RingsGame game = new RingsGame(player, opponent);

                            int port = server.port + 1;
                            for (int i = 0; i < gameList.Count; i++)
                            {
                                if (gameList[i].receivePort != port)
                                    break;
                                else
                                    port+=3;
                            }

                            game.Start(port, this);
                            gameList.Add(game);
                            DistributeMessage(new ChatMessagePacket(null, DateTime.Now.ToString("h:mm:ss"), player.name + " and " + opponent.name + " have entered a game!"));
                        }

                        //Return players to lobby and inform player of rejected invite
                        else
                        {
                            player.state = gameState.Lobby;
                            RefreshPlayerList();
                            Networking.SendPacketTCP(new NetworkStream(player.socket, true), new WindowMessagePacket("Invite Rejected", opponent.name + " rejected your invite\nReturning to lobby"));
                        }
                    }
                    break;
                }
            }
        }

        public override void DistributeMessage(ChatMessagePacket chatMessagePacket)
        {
            foreach (Player player in playerList)
            {
                Networking.SendPacketTCP(new NetworkStream(player.socket, true), chatMessagePacket);
            }
        }
    }
}
