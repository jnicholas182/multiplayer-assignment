﻿using Networking_Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace Server_Core
{
    public class BaseServer<T> where T : BaseGame
    {
        protected TcpListener _tcpListener;
        protected BaseLobby<T> lobby;
        bool Loopback;
        public string banner;
        public int port;
        public int maxPlayerCount;

        public BaseServer(string filename)
        {
            LoadConfig(filename);
            IPAddress ipAddress = Loopback ? IPAddress.Loopback : LocalIPAddress();
            _tcpListener = new TcpListener(ipAddress, port);
        }

        public void LoadConfig(string fileName)
        {
            using(XmlReader reader = XmlReader.Create(@fileName))
            {
                reader.ReadToFollowing("Loopback");
                reader.MoveToFirstAttribute();
                this.Loopback = XmlConvert.ToBoolean(reader.ReadElementContentAsString());

                reader.ReadToFollowing("Banner");
                reader.MoveToFirstAttribute();
                string unpaddedBanner = reader.ReadElementContentAsString();
                this.banner = unpaddedBanner.PadLeft(unpaddedBanner.Length + ((25-unpaddedBanner.Length)/2));

                reader.ReadToFollowing("Port");
                reader.MoveToFirstAttribute();
                this.port = Convert.ToInt32(reader.ReadElementContentAsString());

                reader.ReadToFollowing("PlayerMax");
                reader.MoveToFirstAttribute();
                this.maxPlayerCount = Convert.ToInt32(reader.ReadElementContentAsString());
            }
        }

        public void Start()
        {
            try
            {
                _tcpListener.Start();
                lobby.Start();
                while (true)
                {
                    Socket socket = _tcpListener.AcceptSocket();
                    if (lobby.playerList.Count() < maxPlayerCount)
                    {
                        Networking.SendPacketTCP(new NetworkStream(socket, true), new BooleanPacket(true));
                        new Thread(new ParameterizedThreadStart(AcceptPlayer)).Start(socket);
                    }
                    else
                        Networking.SendPacketTCP(new NetworkStream(socket, true), new BooleanPacket(false));
                }
            }
            catch(Exception)
            {
                Console.WriteLine("=========================\n        Error!        \n=========================\n\nError starting server, there might be a server running already!");
                Console.WriteLine("Press enter to close...");
                Console.ReadLine();
            }
        }

        public void AcceptPlayer(object obj)
        {
            Socket socket = obj as Socket;
            if (socket != null)
            {
                try
                {
                    Player player = new Player(socket, lobby.CheckNameValid(socket));
                    lobby.playerList.Add(player);
                    lobby.RefreshPlayerList();
                    lobby.DistributeMessage(new ChatMessagePacket(null, DateTime.Now.ToString("h:mm:ss"), player.name + " has joined the lobby!"));
                    new Thread(new ParameterizedThreadStart(ReceiveMessage)).Start(player);
                }
                catch (IOException) { }
                catch (NullReferenceException) { }
                catch (Exception e)
                {
                    Console.WriteLine("Error occured: " + e.Message);
                    socket.Close();
                }
            }
        }

        public void Stop()
        {
            _tcpListener.Stop();
            lobby.Stop();
        }

        private IPAddress LocalIPAddress()
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                return null;
            }

            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

            return host
                .AddressList
                .LastOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
        }

        protected virtual void ReceiveMessage(object obj) { }
    }
}