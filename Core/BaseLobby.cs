﻿using Networking_Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server_Core
{
    public class BaseLobby<T> where T : BaseGame
    {
        public List<Player> playerList;
        public List<T> gameList;
        public BaseServer<T> server;
        public string banner;

        public BaseLobby(BaseServer<T> server)
        {
            this.server = server;
            this.banner = server.banner;
            playerList = new List<Player>();
            gameList = new List<T>();
        }

        public void Start()
        {
            RefreshPlayerList();
        }

        public void Stop()
        {
        }

        public virtual void HandleLobbyPacket(DataPacket data, Player player) { }

        protected virtual void HandleBooleanPacket(BooleanPacket booleanPacket, Player player) { }

        protected virtual void HandlePlayerChangeState(ChangeStatePacket changeStatePacket, Player player) { }

        protected virtual void StartGame(Player player, InvitePacket requestGameStartPacket) { }

        public string CheckNameValid(Socket socket)
        {
            bool validName = false;
            UserJoinPacket userJoinPacket = new UserJoinPacket();
            while (!validName)
            {
                validName = true;
                userJoinPacket = Networking.ReceivePacketTCP(new NetworkStream(socket, true)) as UserJoinPacket;
                foreach (Player player in playerList)
                {
                    if (player.name == userJoinPacket.username)
                    {
                        validName = false;
                        Networking.SendPacketTCP(new NetworkStream(socket, true), new BooleanPacket(false));
                        break;
                    }
                }
            }
            Networking.SendPacketTCP(new NetworkStream(socket, true), new BooleanPacket(true));

            return userJoinPacket.username;
        }

        public void RefreshPlayerList()
        {
            foreach (Player player in playerList)
            {
                Networking.SendPacketTCP(new NetworkStream(player.socket, true), new PlayerListPacket(playerList));
            }


            Console.Clear(); Console.WriteLine("=========================\n" + banner + "\n=========================\nName\t\tState\n-------------------------");
            foreach (Player player in playerList)
            {
                Console.Write(player.name + "\t\t");
                if (player.state == gameState.Lobby)
                    Console.ForegroundColor = ConsoleColor.Green;
                else if(player.state == gameState.Pending)
                    Console.ForegroundColor = ConsoleColor.Yellow;
                else if(player.state == gameState.Game)
                    Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine(player.state);
                Console.ResetColor();
            }
        }

        public virtual void DistributeMessage(ChatMessagePacket chatMessagePacket) { }
    }
}
