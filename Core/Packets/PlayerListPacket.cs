﻿using Networking_Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server_Core
{
    [Serializable]
    public class PlayerListPacket : DataPacket
    {
        public List<Player> playerList;

        public PlayerListPacket() { }
        public PlayerListPacket(List<Player> playerList)
        { this.playerList = playerList; }
    }
}
