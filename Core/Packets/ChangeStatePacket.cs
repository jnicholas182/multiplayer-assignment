﻿using Networking_Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server_Core
{
    [Serializable]
    public class ChangeStatePacket : DataPacket
    {
        public gameState state;

        public ChangeStatePacket() { }
        public ChangeStatePacket(gameState state)
        { this.state = state; }
    }
}
