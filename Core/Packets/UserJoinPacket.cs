﻿using Networking_Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server_Core
{
    [Serializable]
    public class UserJoinPacket : DataPacket
    {
        public string username;

        public UserJoinPacket() { }
        public UserJoinPacket(string username)
        { this.username = username; }
    }
}
