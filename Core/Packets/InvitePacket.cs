﻿using Networking_Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server_Core
{
    [Serializable]
    public class InvitePacket : DataPacket
    {
        public string opponent;
        public InvitePacket() { }
        public InvitePacket(string opponent)
        { this.opponent = opponent; }
    }
}
