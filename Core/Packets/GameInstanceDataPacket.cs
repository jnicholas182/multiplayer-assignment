﻿using Networking_Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server_Core
{
    [Serializable]
    public class GameInstanceDataPacket : DataPacket
    {
        public int ID, sendPort, receivePort;
        public GameInstanceDataPacket() { }
        public GameInstanceDataPacket(int ID, int sendPort, int receivePort)
        { this.ID = ID; this.sendPort = sendPort; this.receivePort = receivePort; }
    }
}
