﻿using Networking_Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server_Core
{
    public class BaseGame
    {
        public Player instigator, opponent;
        public UdpClient listener;
        public IPEndPoint receiveEP, sendPlayer1EP, sendPlayer2EP;
        public Socket sendingSocket;
        public int receivePort, player1SendPort, player2SendPort;
        public bool gameOver;

        public BaseGame(Player instigator, Player opponent)
        {
            this.instigator = instigator; this.opponent = opponent;
            instigator.game = this; opponent.game = this;
            gameOver = false;
        }

        public virtual void Start(int receivePort)
        {
            this.receivePort = receivePort;
            player1SendPort = receivePort + 1;
            player2SendPort = receivePort + 2;

            listener = new UdpClient(receivePort);
            receiveEP = new IPEndPoint(IPAddress.Any, receivePort);

            sendingSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            sendPlayer1EP = new IPEndPoint(instigator.ipAddress, player1SendPort);
            sendPlayer2EP = new IPEndPoint(opponent.ipAddress, player2SendPort);


            Networking.SendPacketTCP(new NetworkStream(instigator.socket, true), new GameInstanceDataPacket(1, receivePort, player1SendPort));
            Networking.SendPacketTCP(new NetworkStream(opponent.socket, true), new GameInstanceDataPacket(2, receivePort, player2SendPort));

            new Thread(new ThreadStart(ReceiveMessage)).Start();
        }

        protected virtual void ReceiveMessage() {}

        public virtual void ShutdownGame() { }
    }
}
