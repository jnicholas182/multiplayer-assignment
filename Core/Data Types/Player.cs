﻿using Networking_Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server_Core
{
    public enum gameState
    {
        Lobby,
        Pending,
        Game,
        Quit
    };

    [Serializable]
    public class Player
    {
        public gameState state;
        public string name;
        [NonSerialized]
        public Thread receiveThread;
        [NonSerialized]
        public Socket socket;
        [NonSerialized]
        public IPAddress ipAddress;
        [NonSerialized]
        public BaseGame game;

        public Player(Socket socket, string name)
        {
            this.socket = socket;
            this.ipAddress = (socket.LocalEndPoint as IPEndPoint).Address;
            this.name = name;
            state = gameState.Lobby;
        }
    }
}
